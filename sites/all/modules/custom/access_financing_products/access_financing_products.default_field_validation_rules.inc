<?php
/**
 * @file
 * access_financing_products.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function access_financing_products_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Validación del formato del código de la caja';
  $rule->name = 'box_code_financing_products_case';
  $rule->field_name = 'field_codigo_caja';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'financing_products_case_file';
  $rule->validator = 'field_validation_pcre_validator';
  $rule->settings = array(
    'data' => '/PNPA[0-9]{6}/',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
      7 => 0,
      8 => 0,
      9 => 0,
      10 => 0,
      11 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = '[field-name] - valor inválido: [value]. Revisar formato: PNPA000000';
  $export['box_code_financing_products_case'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Validación del formato del código del expediente';
  $rule->name = 'case_file_code_financing_prod';
  $rule->field_name = 'field_codigo_expediente';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'financing_products_case_file';
  $rule->validator = 'field_validation_pcre_validator';
  $rule->settings = array(
    'data' => '/F[0-9]{8}/',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
      5 => 0,
      6 => 0,
      7 => 0,
      8 => 0,
      9 => 0,
      10 => 0,
      11 => 0,
    ),
    'errors' => 1,
    'condition' => 0,
    'condition_wrapper' => array(
      'condition_field' => '',
      'condition_operator' => 'equals',
      'condition_value' => '',
    ),
  );
  $rule->error_message = '[field-name] - valor inválido: [value]. Revisar formato: F00000000';
  $export['case_file_code_financing_prod'] = $rule;

  return $export;
}
