<?php
/**
 * @file
 * access_financing_products.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function access_financing_products_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'clear clients_financing_products feeds'.
  $permissions['clear clients_financing_products feeds'] = array(
    'name' => 'clear clients_financing_products feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'create financing_products_case_file content'.
  $permissions['create financing_products_case_file content'] = array(
    'name' => 'create financing_products_case_file content',
    'roles' => array(
      'operador-productos-financiacion' => 'operador-productos-financiacion',
      'supervisor' => 'supervisor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create financing_products_client content'.
  $permissions['create financing_products_client content'] = array(
    'name' => 'create financing_products_client content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any financing_products_case_file content'.
  $permissions['delete any financing_products_case_file content'] = array(
    'name' => 'delete any financing_products_case_file content',
    'roles' => array(
      'supervisor' => 'supervisor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any financing_products_client content'.
  $permissions['delete any financing_products_client content'] = array(
    'name' => 'delete any financing_products_client content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own financing_products_case_file content'.
  $permissions['delete own financing_products_case_file content'] = array(
    'name' => 'delete own financing_products_case_file content',
    'roles' => array(
      'supervisor' => 'supervisor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own financing_products_client content'.
  $permissions['delete own financing_products_client content'] = array(
    'name' => 'delete own financing_products_client content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any financing_products_case_file content'.
  $permissions['edit any financing_products_case_file content'] = array(
    'name' => 'edit any financing_products_case_file content',
    'roles' => array(
      'supervisor' => 'supervisor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any financing_products_client content'.
  $permissions['edit any financing_products_client content'] = array(
    'name' => 'edit any financing_products_client content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own financing_products_case_file content'.
  $permissions['edit own financing_products_case_file content'] = array(
    'name' => 'edit own financing_products_case_file content',
    'roles' => array(
      'supervisor' => 'supervisor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own financing_products_client content'.
  $permissions['edit own financing_products_client content'] = array(
    'name' => 'edit own financing_products_client content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'import clients_financing_products feeds'.
  $permissions['import clients_financing_products feeds'] = array(
    'name' => 'import clients_financing_products feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'tamper clients_financing_products'.
  $permissions['tamper clients_financing_products'] = array(
    'name' => 'tamper clients_financing_products',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'unlock clients_financing_products feeds'.
  $permissions['unlock clients_financing_products feeds'] = array(
    'name' => 'unlock clients_financing_products feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
