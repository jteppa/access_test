<?php
/**
 * @file
 * access_financing_products.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function access_financing_products_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'clients_financing_products';
  $feeds_importer->config = array(
    'name' => 'Clientes de Productos de financiación',
    'description' => 'Importador de clientes de Productos de financiación',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'delete_uploaded_file' => 0,
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Numero_de_Identificacion_Grupo_Economico',
            'target' => 'field_cve_ref_identi',
            'unique' => FALSE,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'Nombre_del_Producto',
            'target' => 'field_product_name',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Numero_de_CIS_Grupo_Economico',
            'target' => 'field_number_cis_economic_group',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'Status',
            'target' => 'field_st_cartera',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Comentarios',
            'target' => 'field_comments',
            'unique' => FALSE,
            'language' => 'und',
          ),
          5 => array(
            'source' => 'Numero_Grupo_Economico',
            'target' => 'field_economic_group_number',
            'unique' => FALSE,
            'language' => 'und',
          ),
          6 => array(
            'source' => 'Nombre_Grupo_Economico',
            'target' => 'field_economic_group_name',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'financing_products_client',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['clients_financing_products'] = $feeds_importer;

  return $export;
}
