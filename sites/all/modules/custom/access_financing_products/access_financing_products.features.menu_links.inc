<?php
/**
 * @file
 * access_financing_products.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function access_financing_products_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-hipotecas_generar-reporte-de-productos-de-financiacin:financing-products/dates-report.
  $menu_links['menu-hipotecas_generar-reporte-de-productos-de-financiacin:financing-products/dates-report'] = array(
    'menu_name' => 'menu-hipotecas',
    'link_path' => 'financing-products/dates-report',
    'router_path' => 'financing-products/dates-report',
    'link_title' => 'Generar Reporte de Productos de Financiación',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hipotecas_generar-reporte-de-productos-de-financiacin:financing-products/dates-report',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-hipotecas_productos-de-financiacin:<nolink>',
  );
  // Exported menu link: menu-hipotecas_importar-clientes-de-productos-de-financiacin:import/clients_financing_products.
  $menu_links['menu-hipotecas_importar-clientes-de-productos-de-financiacin:import/clients_financing_products'] = array(
    'menu_name' => 'menu-hipotecas',
    'link_path' => 'import/clients_financing_products',
    'router_path' => 'import/%',
    'link_title' => 'Importar Clientes de Productos de Financiación',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hipotecas_importar-clientes-de-productos-de-financiacin:import/clients_financing_products',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-hipotecas_productos-de-financiacin:<nolink>',
  );
  // Exported menu link: menu-hipotecas_productos-de-financiacin:<nolink>.
  $menu_links['menu-hipotecas_productos-de-financiacin:<nolink>'] = array(
    'menu_name' => 'menu-hipotecas',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Productos de Financiación',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-hipotecas_productos-de-financiacin:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-operador---hipotecas_procesar-productos-de-financiacin:financing-products/process.
  $menu_links['menu-operador---hipotecas_procesar-productos-de-financiacin:financing-products/process'] = array(
    'menu_name' => 'menu-operador---hipotecas',
    'link_path' => 'financing-products/process',
    'router_path' => 'financing-products/process',
    'link_title' => 'Procesar Productos de financiación',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-operador---hipotecas_procesar-productos-de-financiacin:financing-products/process',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 1,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: navigation_productos-de-financiacin---cliente:node/add/financing-products-client.
  $menu_links['navigation_productos-de-financiacin---cliente:node/add/financing-products-client'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/financing-products-client',
    'router_path' => 'node/add/financing-products-client',
    'link_title' => 'Productos de Financiación - Cliente',
    'options' => array(
      'identifier' => 'navigation_productos-de-financiacin---cliente:node/add/financing-products-client',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Exported menu link: navigation_productos-de-financiacin---expediente:node/add/financing-products-case-file.
  $menu_links['navigation_productos-de-financiacin---expediente:node/add/financing-products-case-file'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/financing-products-case-file',
    'router_path' => 'node/add/financing-products-case-file',
    'link_title' => 'Productos de Financiación - Expediente',
    'options' => array(
      'identifier' => 'navigation_productos-de-financiacin---expediente:node/add/financing-products-case-file',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Generar Reporte de Productos de Financiación');
  t('Importar Clientes de Productos de Financiación');
  t('Procesar Productos de financiación');
  t('Productos de Financiación');
  t('Productos de Financiación - Cliente');
  t('Productos de Financiación - Expediente');

  return $menu_links;
}
