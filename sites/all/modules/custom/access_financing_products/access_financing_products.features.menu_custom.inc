<?php
/**
 * @file
 * access_financing_products.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function access_financing_products_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-hipotecas.
  $menus['menu-hipotecas'] = array(
    'menu_name' => 'menu-hipotecas',
    'title' => 'Supervisor',
    'description' => '',
  );
  // Exported menu: menu-operador---hipotecas.
  $menus['menu-operador---hipotecas'] = array(
    'menu_name' => 'menu-operador---hipotecas',
    'title' => 'Operador',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Operador');
  t('Supervisor');

  return $menus;
}
