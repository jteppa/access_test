<?php
/**
 * @file
 * access_financing_products.features.conditional_fields.inc
 */

/**
 * Implements hook_conditional_fields_default_fields().
 */
function access_financing_products_conditional_fields_default_fields() {
  $items = array();

  $items["node:financing_products_case_file"] = array(
    array(
      'entity' => 'node',
      'bundle' => 'financing_products_case_file',
      'dependent' => 'field_codigo_expediente',
      'dependee' => 'field_status',
      'options' => array(
        'state' => 'required',
        'condition' => 'value',
        'grouping' => 'AND',
        'effect' => FALSE,
        'effect_options' => array(),
        'element_view' => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        'element_view_per_role' => 0,
        'element_view_roles' => array(
          1 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          2 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          3 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          4 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          5 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          6 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          7 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          8 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          9 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          10 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          11 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
        ),
        'element_edit' => array(
          1 => 1,
          3 => 0,
        ),
        'element_edit_per_role' => 0,
        'element_edit_roles' => array(
          1 => array(
            1 => 1,
            3 => 0,
          ),
          2 => array(
            1 => 1,
            3 => 0,
          ),
          3 => array(
            1 => 1,
            3 => 0,
          ),
          4 => array(
            1 => 1,
            3 => 0,
          ),
          5 => array(
            1 => 1,
            3 => 0,
          ),
          6 => array(
            1 => 1,
            3 => 0,
          ),
          7 => array(
            1 => 1,
            3 => 0,
          ),
          8 => array(
            1 => 1,
            3 => 0,
          ),
          9 => array(
            1 => 1,
            3 => 0,
          ),
          10 => array(
            1 => 1,
            3 => 0,
          ),
          11 => array(
            1 => 1,
            3 => 0,
          ),
        ),
        'selector' => '',
        'values_set' => 1,
        'value_form' => array(
          0 => array(
            'value' => 'Activo',
          ),
        ),
        'value' => array(
          0 => array(
            'value' => 'Activo',
          ),
        ),
        'values' => array(),
      ),
    ),
    array(
      'entity' => 'node',
      'bundle' => 'financing_products_case_file',
      'dependent' => 'field_codigo_expediente',
      'dependee' => 'field_status',
      'options' => array(
        'state' => 'visible',
        'condition' => 'value',
        'grouping' => 'AND',
        'effect' => 'show',
        'effect_options' => array(),
        'element_view' => array(
          1 => 1,
          2 => 2,
          5 => 0,
          3 => 0,
          4 => 0,
        ),
        'element_view_per_role' => 0,
        'element_view_roles' => array(
          1 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          2 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          3 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          4 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          5 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          6 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          7 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          8 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          9 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          10 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
          11 => array(
            1 => 1,
            2 => 2,
            5 => 0,
            3 => 0,
            4 => 0,
          ),
        ),
        'element_edit' => array(
          1 => 1,
          3 => 0,
        ),
        'element_edit_per_role' => 0,
        'element_edit_roles' => array(
          1 => array(
            1 => 1,
            3 => 0,
          ),
          2 => array(
            1 => 1,
            3 => 0,
          ),
          3 => array(
            1 => 1,
            3 => 0,
          ),
          4 => array(
            1 => 1,
            3 => 0,
          ),
          5 => array(
            1 => 1,
            3 => 0,
          ),
          6 => array(
            1 => 1,
            3 => 0,
          ),
          7 => array(
            1 => 1,
            3 => 0,
          ),
          8 => array(
            1 => 1,
            3 => 0,
          ),
          9 => array(
            1 => 1,
            3 => 0,
          ),
          10 => array(
            1 => 1,
            3 => 0,
          ),
          11 => array(
            1 => 1,
            3 => 0,
          ),
        ),
        'selector' => '',
        'values_set' => 1,
        'value_form' => array(
          0 => array(
            'value' => 'Activo',
          ),
        ),
        'value' => array(
          0 => array(
            'value' => 'Activo',
          ),
        ),
        'values' => array(),
      ),
    ),
  );

  return $items;
}
