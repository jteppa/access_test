<?php
/**
 * @file
 * access_financing_products.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function access_financing_products_user_default_roles() {
  $roles = array();

  // Exported role: Gods.
  $roles['Gods'] = array(
    'name' => 'Gods',
    'weight' => 3,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: operador-productos-financiacion.
  $roles['operador-productos-financiacion'] = array(
    'name' => 'operador-productos-financiacion',
    'weight' => 10,
  );

  // Exported role: supervisor.
  $roles['supervisor'] = array(
    'name' => 'supervisor',
    'weight' => 6,
  );

  return $roles;
}
