<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf12caf2b3a6a5eceb4c84e22bcb8150b
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf12caf2b3a6a5eceb4c84e22bcb8150b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf12caf2b3a6a5eceb4c84e22bcb8150b::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
