<?php
/**
 * @file
 * access_financing_products.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function access_financing_products_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_datos_del_cliente|node|financing_products_client_case|form';
  $field_group->group_name = 'group_datos_del_cliente';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'financing_products_case_file';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Datos del Producto de Financiación',
    'weight' => '4',
    'children' => array(
      0 => 'field_comments',
      1 => 'field_economic_group_name',
      2 => 'field_economic_group_number',
      3 => 'field_number_cis_economic_group',
      4 => 'field_cve_ref_identi',
      5 => 'field_st_cartera',
      6 => 'field_financing_product',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Datos del Producto de Financiación',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_datos_del_cliente|node|financing_products_client_case|form'] = $field_group;

  return $export;
}
